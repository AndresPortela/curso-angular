import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinoViaje.Models.TsComponent } from './destino-viaje.models.ts.component';

describe('DestinoViaje.Models.TsComponent', () => {
  let component: DestinoViaje.Models.TsComponent;
  let fixture: ComponentFixture<DestinoViaje.Models.TsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinoViaje.Models.TsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinoViaje.Models.TsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
